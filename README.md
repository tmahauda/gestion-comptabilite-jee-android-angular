# Gestion de comptabilité commune entre amis

<div align="center">
<img width="500" height="400" src="Comptabilite.jpg">
</div>

## Description du projet

Applications web et mobile réalisées avec Spring côté serveur, Angular et Android côté client en LP MIAR à l'IUT de Nantes dans le cadre du module "Projet Alternants" durant l'année 2018-2019 avec un groupe de six personnes.

Elles permettent de gérer une "comptabilité commune" lors de l’organisation d’événements entre amis.

Exemple. Alice, Bob et Charlie décident d’organiser un week-end au bord de la mer. Alice paie la location AirBnB pour 2 nuits (80e). Bob s’occupe des courses (40e). \
Bob (qui conduit Alice et Charlie) paie également l’essence pour le trajet (40e). Lors du week-end, Charlie paie le restaurant (60e), Bob, quant à lui paie également des glaces (20e), etc. \
L’idée de l’application est que chacun saisisse ses dépenses liées à l’événement, et que l’application calcule automatiquement le coût total de l’événement, le coût par personne, le solde de chacun, etc. \
Dans notre exemple, Alice a dépensé 80e, Bob 100e et Charlie 60e. Le coût total de l’événement est de 240e. Le coût par personne est de 80e. Le solde d’Alice est de 0e, le solde Bob est de +20e et celui de Charlie est de −20e.

Dans ce projet, nous avons continué le développement d’une application mobile et nous avons initié le développement "from scratch" d'une application web permettant à des utilisateurs de créer
des événements, d’ajouter des participants à un événement, de saisir des dépenses liées à un événement, de calculer le solde de chaque événement, etc. Bien évidemment, cette application
est "répartie", nous avons également continué le développement du serveur web qui fournis une API RESTfull permettant aux applications clientes d’interagir.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de six étudiants de l'IUT de Nantes :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr ;
- Cédric HAMEL : cedric.hamel@etu.univ-nantes.fr ;
- Corentin METROT : corentin.metrot@etu.univ-nantes.fr ;
- Quentin HESRY : quentin.hesry@etu.univ-nantes.fr ;
- Tristan COUTURIER : tristan.couturier@etu.univ-nantes.fr ;
- Valentin BOUDEAU : valentin.boudeau@etu.univ-nantes.fr.

### Encadrants

Ce projet fut encadré par deux enseignants de l'IUT de Nantes :
- Arnaud LANOIX BRAUER : arnaud.lanoix@univ-nantes.fr ;
- Jean-François BERDJUGIN – jean-Francois.berdjugin@univ-nantes.fr.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Projet Alternants" de la LP MIAR.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2019 sur une période de deux semaines pendant les heures de projet et personnelles à la maison. \
Il a été terminé et rendu le 03/05/2019.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java EE ;
- Eclipse ;
- Visual Studio Code ;
- Maven ;
- GitLab ;
- Spring ;
- API RESTfull ;
- Android ;
- Angular ;
- SGBD MySQL ;
- Docker.

## Objectifs

Trois développements a été demandé :
- Poursuivre le développement du serveur REST ;
- Poursuivre le développement de l’application native Android ;
- Développer l’application web cliente, en utilisant des tehcnologies web "modernes", à savoir Angular.