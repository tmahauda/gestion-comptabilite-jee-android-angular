package com.example.projet.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.projet.R;

import java.util.HashMap;

public class RemboursementEventAdapter extends ArrayAdapter<Evenement> {

    private int myItemLayout;
    private LayoutInflater li;
    private HashMap<String, String> mapEventOwing;

    public RemboursementEventAdapter(Context context, int resourceId) {
        super(context, resourceId);
        this.mapEventOwing = new HashMap<>();
        this.myItemLayout = resourceId;
        this.li = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null)
        {
            view = this.li.inflate(this.myItemLayout, null);
        }

        Evenement v = this.getItem(position);
        if(v != null)
        {

            TextView nomEvenement = view.findViewById(R.id.nomEvenement);
            TextView aRembourserPlus = view.findViewById(R.id.aRembourserPlus);
            TextView aRembourserMoins = view.findViewById(R.id.aRembourserMoins);

            nomEvenement.setText(v.getNom());
            if(v.getId() != null && mapEventOwing.containsKey(v.getId())) {
                Double value = Double.valueOf(mapEventOwing.get(v.getId()));
                if (value == -0) {
                    aRembourserPlus.setText(String.valueOf(0));
                    aRembourserMoins.setText(String.valueOf(0));
                } else if (value < 0) {
                    aRembourserPlus.setText(String.valueOf(Math.abs(value)));
                    aRembourserMoins.setText(String.valueOf(0));
                } else {
                    aRembourserMoins.setText(String.valueOf(Math.abs(value)));
                    aRembourserPlus.setText(String.valueOf(0));
                }
            }

        }
        return view;
    }

    public void addOnOwingMap(String idEvent, String montant){
        if(!this.mapEventOwing.containsKey(idEvent)){
            this.mapEventOwing.put(idEvent, montant);
        }
    }

    public void clearOwingMap(){
        this.mapEventOwing.clear();
    }
}
