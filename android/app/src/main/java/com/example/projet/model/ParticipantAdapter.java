package com.example.projet.model;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.example.projet.R;

public class ParticipantAdapter extends ArrayAdapter<User> {

    private Context mContext;
    private List<User> participantList;

    public ParticipantAdapter(@NonNull Context context, @SuppressLint("SupportAnnotationUsage") @LayoutRes ArrayList<User> list) {
        super(context, 0 , list);
        mContext = context;
        participantList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.item_event_participants,parent,false);

        User user = participantList.get(position);

        ImageView image = listItem.findViewById(R.id.image_view);
        TextView name = listItem.findViewById(R.id.name);
        image.setImageDrawable(listItem.getResources().getDrawable(R.drawable.ic_user));
        name.setText(user.getNom());

        return listItem;
    }
}