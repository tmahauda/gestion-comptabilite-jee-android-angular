package com.example.projet;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.projet.model.Evenement;
import com.example.projet.model.EventAdapter;
import com.example.projet.model.RemboursementEventAdapter;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Iterator;

public class RemboursementActivity extends AppCompatActivity {

    private static final String MY_PREFS_NAME = MenuActivity.MY_PREFS_NAME;
    SharedPreferences prefs;
    ListView remboursementListView;
    RemboursementEventAdapter adapterRemboursement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remboursement);

        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        remboursementListView = findViewById(R.id.listViewRemboursement);
        adapterRemboursement = new RemboursementEventAdapter(this, R.layout.item_remboursement_list_view);
        remboursementListView.setAdapter(adapterRemboursement);
    }

    @Override
    protected void onStart() {
        super.onStart();

        final ArrayList<Evenement> resultEvent = new ArrayList<>();
        this.adapterRemboursement.clearOwingMap();
        String url = getString(R.string.urlHttp)+"/api/evenement/getAll/user/"+prefs.getString("id","");
        Ion.with(getApplicationContext())
                .load(url)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        if (result == null) {
                            Toast.makeText(getApplicationContext(), "Pas d'invitations", Toast.LENGTH_SHORT).show();
                        } else {
                            adapterRemboursement.clearOwingMap();
                            Iterator it = result.iterator();
                            while (it.hasNext()) {
                                JsonObject event = (JsonObject) it.next();
                                JsonObject creator = (JsonObject) event.get("user");
                                System.out.println(event);
                                resultEvent.add(new Evenement(event.get("id").getAsString(),
                                        event.get("title").getAsString(),
                                        event.get("date").getAsString(), event.get("place").getAsString(),
                                        "blablabla", creator.get("username").getAsString()));
                            }
                            RemboursementActivity.this.populate(resultEvent, true);
                        }
                        for(final Evenement event : resultEvent) {
                            String url3 = getString(R.string.urlHttp) + "/api/owing/get/" + prefs.getString("id", "") + "/" + event.getId();
                            Ion.with(getApplicationContext())
                                    .load(url3)
                                    .asJsonObject()
                                    .setCallback(new FutureCallback<JsonObject>() {
                                        @Override
                                        public void onCompleted(Exception e, JsonObject result) {
                                            Double bal = result.get("owing").getAsDouble();
                                            bal *= -1;
                                            Log.i("owingGetAll", ""+ event.getId() + " " + bal.toString());
                                            java.text.DecimalFormat df = new java.text.DecimalFormat("0.##");
                                            adapterRemboursement.addOnOwingMap(event.getId(), df.format(bal));
                                            adapterRemboursement.notifyDataSetChanged();
                                        }
                                    });
                        }
                    }
                });

        String url2 = getString(R.string.urlHttp)+"/api/evenement/getAll/userCreator/"+prefs.getString("id","");
        Ion.with(this)
                .load(url2)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {

                        Iterator it = result.iterator();
                        while (it.hasNext()){
                            JsonObject event = (JsonObject) it.next();

                            resultEvent.add(
                                    new Evenement
                                            (event.get("id").getAsString(),
                                                    event.get("title").getAsString(),
                                                    event.get("date").getAsString(),
                                                    event.get("place").getAsString(),
                                                    event.get("description").getAsString(),
                                                    prefs.getString("pseudo","")
                                            ));
                        }
                        RemboursementActivity.this.populate(resultEvent, false);
                        for(final Evenement event : resultEvent) {
                            String url3 = getString(R.string.urlHttp) + "/api/owing/get/" + prefs.getString("id", "") + "/" + event.getId();
                            Ion.with(getApplicationContext())
                                    .load(url3)
                                    .asJsonObject()
                                    .setCallback(new FutureCallback<JsonObject>() {
                                        @Override
                                        public void onCompleted(Exception e, JsonObject result) {
                                            Double bal = result.get("owing").getAsDouble();
                                            bal *= -1;
                                            Log.i("owingGetAll", ""+ event.getId() + " " + bal.toString());
                                            java.text.DecimalFormat df = new java.text.DecimalFormat("0.##");
                                            adapterRemboursement.addOnOwingMap(event.getId(), df.format(bal));
                                            adapterRemboursement.notifyDataSetChanged();
                                        }
                                    });
                        }
                    }
                });


    }

    public void populate(ArrayList<Evenement> listEvent, boolean clear){
        if(clear){
            this.adapterRemboursement.clear();
        }
        this.adapterRemboursement.addAll(listEvent);
        this.adapterRemboursement.notifyDataSetChanged();
    }
}
