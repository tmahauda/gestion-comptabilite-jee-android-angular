package com.example.projet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.projet.model.ParticipantAdapter;
import com.example.projet.model.User;

import java.util.ArrayList;

public class affichage_participant_activity extends AppCompatActivity {

    ParticipantAdapter participantAdapter;
    ListView listView;
    Button finishButton;
    String eventID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affichage_participant_activity);

        Intent intent = getIntent();
        eventID = (String) intent.getExtras().get("eventID");
        ArrayList<User> participantList = (ArrayList<User>) intent.getExtras().getSerializable("participant");

        finishButton = findViewById(R.id.retour);
        listView = findViewById(R.id.participant_recycle_view);

        participantAdapter = new ParticipantAdapter(getApplicationContext(), participantList);
        listView.setAdapter(participantAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User tmp = (User) parent.getItemAtPosition(position);
                Intent detailPseudo = new Intent(getApplicationContext(), details_pseudo_event.class);
                detailPseudo.putExtra("eventid", eventID);
                detailPseudo.putExtra("pseudoid", tmp.getId());
                startActivity(detailPseudo);
            }
        });
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
