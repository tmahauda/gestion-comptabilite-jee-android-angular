import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentsModule } from './components/components.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AccueilComponent } from './accueil/accueil.component';
import { MaterialModule } from './material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ListEventsComponent } from './list-events/list-events.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { MyEventsComponent } from './my-events/my-events.component';
import { ProfilComponent } from './profil/profil.component';
import { UserComponent } from './user/user.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { HttpClientModule } from '@angular/common/http';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { InscriptionComponent } from './inscription/inscription.component';
import { DatePipe } from '@angular/common';
import { Ng2ImgMaxModule } from 'ng2-img-max';

import { Event } from './model/event';
import { Expense } from './model/expense';
import { Owing } from './model/owing';
import { User } from './model/user';
import { DetailEventsComponent } from './detail-events/detail-events.component';

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    ListEventsComponent,
    CreateEventComponent,
    MyEventsComponent,
    ProfilComponent,
    UserComponent,
    ConnexionComponent,
    InscriptionComponent,
    DetailEventsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ComponentsModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    Ng2ImgMaxModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    DatePipe,
    User,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
