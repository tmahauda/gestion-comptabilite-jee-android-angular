import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { MaterialService } from './material.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { GenericDialogComponent } from './components/generic-dialog/generic-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent implements OnInit {
  route: string;
  username = localStorage.getItem('pseudoUser');
  usermail = localStorage.getItem('emailUser');
  userimage = localStorage.getItem('imageUser');
  connected = false;

  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private materialService: MaterialService,
    private dialog: MatDialog,
    location: Location,
    private router: Router,
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    router.events.subscribe(val => {
      if (location.path() !== '') {
        this.route = location.path();
        this.username = localStorage.getItem('pseudoUser');
        this.usermail = localStorage.getItem('emailUser');
        this.userimage = localStorage.getItem('imageUser');
      }
      if (!(this.route === '/connexion' || this.route === '/inscription')) {
        this.connected = true;
      }
    });
  }

  ngOnInit(): void {

  }

  openConfirmationDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(GenericDialogComponent, {
      /** Données paramètre de la modale générique */
      data: {
        message: 'Voulez-vous vraiment vous déconnecter ?',
        exit: 'Annuler',
        validate: 'Se déconnecter',
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'save') {
        /** Exécute une action si l'on a confirmé avec la modale */
        this.disconnect();
      }
    });
  }

  disconnect() {
    localStorage.removeItem('idUser');
    localStorage.removeItem('pseudoUser');
    localStorage.removeItem('emailUser');
    localStorage.removeItem('passwordUser');
    localStorage.removeItem('imageUser');
    this.router.navigate(['/connexion']);
  }
}
