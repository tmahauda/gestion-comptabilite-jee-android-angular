import { Component, OnInit } from '@angular/core';
import { MaterialService } from 'src/app/material.service';
import { EventService } from '../event.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {

  nombreEventAsAdmin: number;
  nombreEventAsParticipant: number;

  constructor(private eventService: EventService, private materialService: MaterialService) { }

  ngOnInit() {
    this.getNombreEventAsAdmin();
    this.getNombreEventAsParticipant();
  }

  private getNombreEventAsAdmin() {
    this.eventService.getUserCreatorEvents(localStorage.getItem('idUser')).subscribe(
      (events) => {
        this.nombreEventAsAdmin = events.length;
      },
      (error) => {
        this.nombreEventAsAdmin = 0;
    });
  }

  private getNombreEventAsParticipant() {
    this.eventService.getUserEvents(localStorage.getItem('idUser')).subscribe(
      (events) => {
        this.nombreEventAsParticipant = events.length;
      },
      (error) => {
        this.nombreEventAsParticipant = 0;
    });
  }

}
