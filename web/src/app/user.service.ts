import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { User } from './model/user';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({ providedIn: 'root' })
export class UserService {

  private usersUrl = '/api/membre/getAll';  // URL to web api
  private userUrl = '/api/membre/get';
  private updateUserUrl = '/api/membre/update';

  constructor(private http: HttpClient) { }

  /** GET users from the server */
  getUsers (): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl)
      .pipe(catchError(this.handleError<User[]>('getUsers', []))
      );
  }

   /** GET user by id. Will 404 if id not found */
   getUser(id: string): Observable<User> {
    const url = `${this.userUrl}/${id}`;
    return this.http.get<User>(url)
    .pipe(catchError(this.handleError<User>(`getUser id=${id}`))
    );
  }

  updateUser(id: string, pseudo: string, email: string, password: string, image: string): Observable<User> {
    const url = `${this.updateUserUrl}/${id}`;
    return this.http.put<User>(url, {pseudo: pseudo, email: email, password: password, image: image}, httpOptions)
    .pipe(catchError(this.handleError<User>(`updateUser id=${id}`))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
