import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  constructor() { }

  isLogin() {
    if (localStorage.getItem('pseudoUser')) {
      return true;
    } else {
      return false;
    }
  }

  logOutAction() {
    localStorage.removeItem('pseudoUser');
  }

}
