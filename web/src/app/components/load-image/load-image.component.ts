import { Ng2ImgMaxService } from 'ng2-img-max';
import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-load-image',
  templateUrl: './load-image.component.html',
  styleUrls: ['./load-image.component.scss']
})
export class LoadImageComponent implements OnInit {

  @Input() public image: string = '';
  @Input() public afficherButton: boolean;

  constructor(
    private imageService: Ng2ImgMaxService
  ) {}

  ngOnInit() {}

  load(event: any) {
    if (event.target.files && event.target.files[0]) {
      const file: File = event.target.files[0];
      const reader: FileReader = new FileReader();

      //On resize l'image
      this.imageService.resizeImage(file, 100, 100).subscribe(
        (newFile) => {
          //Convertit le fichier resize en chaine de caractère
          reader.readAsDataURL(newFile); // read file as data url
          reader.onload = (result: any) => { // called once readAsDataURL is completed
            this.image = result.target.result;
          };
      }, (error) => {
        console.log(error);
      });
    }
  }

}
