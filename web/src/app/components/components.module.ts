import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';
import { GenericDialogComponent } from './generic-dialog/generic-dialog.component';
import { MatDialogModule } from '@angular/material';
import { LoadImageComponent } from './load-image/load-image.component';

@NgModule({
  declarations: [NavbarComponent, GenericDialogComponent, LoadImageComponent],
  imports: [
    BrowserModule,
    CommonModule,
    MaterialModule,
    RouterModule,
    MatDialogModule
  ],
  exports: [
    NavbarComponent,
    LoadImageComponent
  ],
  entryComponents: [GenericDialogComponent]
})
export class ComponentsModule { }
