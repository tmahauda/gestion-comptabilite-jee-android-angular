import { ChangeDetectorRef , Component, OnInit } from '@angular/core';
import { MaterialService } from 'src/app/material.service';
import {MediaMatcher} from '@angular/cdk/layout';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private materialService: MaterialService) {
  }

  ngOnInit(): void {

  }
}
