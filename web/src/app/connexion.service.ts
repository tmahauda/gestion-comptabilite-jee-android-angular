import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConnexionService {

  constructor(private httpClient: HttpClient) { }

  checkUser(email: string, password: string, response: any[]): boolean {
    const passwordResponse = response['password'];
    return password === passwordResponse;
  }

  getUser(email: string, password: string) {
    const check = false;
    const url = '/api/membre/get/email/' + email;

    return this.httpClient.get<any[]>(url);
  }

}
