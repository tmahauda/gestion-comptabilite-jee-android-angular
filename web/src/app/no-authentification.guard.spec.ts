import { TestBed, async, inject } from '@angular/core/testing';

import { NoAuthentificationGuard } from './no-authentification.guard';

describe('NoAuthentificationGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NoAuthentificationGuard]
    });
  });

  it('should ...', inject([NoAuthentificationGuard], (guard: NoAuthentificationGuard) => {
    expect(guard).toBeTruthy();
  }));
});
