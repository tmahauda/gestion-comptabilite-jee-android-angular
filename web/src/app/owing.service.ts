import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
 
import { Owing } from './model/owing';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class OwingService {
  //URL WEB API
  private urlUserOwingByEvent = '/api/owing/get';
 
  constructor(private http: HttpClient) { }

   /** GET expense by id. Will 404 if id not found */
   getExpense(idUser: String, idEvent: String): Observable<Owing> {
    const url = `${this.urlUserOwingByEvent}/${idUser}/${idEvent}`;
    return this.http.get<Owing>(url)
    .pipe(catchError(this.handleError<Owing>(`getExpense idUser=${idUser} idEvent=${idEvent}`))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      console.log('Erreur');
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}