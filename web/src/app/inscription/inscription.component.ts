import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { InscriptionService } from '../inscription.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { CrossFieldErrorMatcher } from '../crossFieldErrorMatcher';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {

  public signInForm: FormGroup;
  CrossFieldErrorMatcher = new CrossFieldErrorMatcher();
  hide = true;
  hideConfirm = true;
  notSame = false;
  
  constructor(
    private inscriptionService: InscriptionService,
    private router: Router,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
  ) {}

  ngOnInit() {
    this.signInForm = this.fb.group({
      pseudo: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      passwordConfirmation: ['']
    }, {
      validator: this.checkPasswords,
    }
    );
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.signInForm.controls[controlName].hasError(errorName);
  }

  public formHasError = (errorName: string) => {
    return this.signInForm.hasError(errorName);
  }

  public onSubmit = () => {
    if (this.signInForm.valid) {
      const pseudo = this.signInForm.value.pseudo;
      const email = this.signInForm.value.email;
      const password = this.signInForm.value.password;
      const image = '';

      this.inscriptionService.putUser(pseudo, email, password, image).subscribe(
        (response) => {
          const action = 'Erreur';
          this.snackBar.open('Inscription confirmée', 'Fermer', {
            duration: 2000,
          });
          this.router.navigate(['/connexion']);
        },
        (error) => {
          const action = 'Erreur';
          this.snackBar.open(error, action, {
            duration: 2000,
          });
        }
      );
    }
  }
  checkPasswords(form: FormGroup) {
    const pass = form.get('password').value;
    const confirmPass = form.get('passwordConfirmation').value;

    return pass === confirmPass ? null : { notSame: true };
  }
}
