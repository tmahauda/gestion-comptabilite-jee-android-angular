import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConnexionService } from '../connexion.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {

  public authForm: FormGroup;
  hide = true;

  constructor(
    private connexionService: ConnexionService,
    private router: Router,
    private snackBar: MatSnackBar,
    ) { }

  ngOnInit() {
    this.authForm = new FormGroup({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.email
      ])),
      password: new FormControl('', [Validators.required])
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.authForm.controls[controlName].hasError(errorName);
  }

  public onSubmit = () => {
    if (this.authForm.valid) {
      const email = this.authForm.value.email;
      const password = this.authForm.value.password;
      this.connexionService.getUser(email, password).subscribe(
        (response) => {
          if (this.connexionService.checkUser(email, password, response)) {
            localStorage.setItem('idUser', response['id']);
            localStorage.setItem('pseudoUser', response['username']);
            localStorage.setItem('emailUser', response['email']);
            localStorage.setItem('passwordUser', response['password']);
            localStorage.setItem('imageUser', response['image']);
            this.router.navigate(['/accueil']);
          }
        },
        (error) => {
          const action = 'Erreur';
          this.snackBar.open(error, action, {
            duration: 2000,
          });
        }
      );
    }
  }
}
