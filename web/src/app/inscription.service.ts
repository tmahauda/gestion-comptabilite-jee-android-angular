import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class InscriptionService {

  constructor(private httpClient: HttpClient) {}
  putUser(pseudo: string, email: string, password: string, image: string){
    const url = '/api/membre/add';
    return this.httpClient.post(url, {pseudo: pseudo, email: email, password: password, image: image}, httpOptions);
  }
}
