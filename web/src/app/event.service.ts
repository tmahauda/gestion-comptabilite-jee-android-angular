import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
 
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
 
import { Event } from './model/event';
import { User } from './model/user';

@Injectable({ providedIn: 'root' })
export class EventService {

  //Header
  private header : HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  //URL WEB API

  //GET
  private urlEvenements : string = '/api/evenement/getAll';
  private urlEvenement : string = '/api/evenement/get';
  private urlMembre : string = '/api/evenement/getAll/user';
  private urlCreateur : string = '/api/evenement/getAll/userCreator';

  //POST
  private urlCreerEvent = '/api/evenement/add';

  //PUT
  private urlAddUserToEvent = '/api/evenement/addUser';
  private urlRemoveUserFromEvent = '/api/evenement/removeUser';

  //DELETE
  private urlRemoveEvent = '/api/evenement/delete';
 
  constructor(private http: HttpClient) { }
 
  /** POST create event */
  addEvent(event : Event) : Observable<Event> {

    const bodyJson = {
      "userId" : event.user.id,
      "title": event.title,
      "date": event.date,
      "place": event.place,
      "description": event.description,
      "image": event.image
    };
    
    return this.http.post<Event>(this.urlCreerEvent, JSON.stringify(bodyJson), {
      headers: this.header,
    })
  }

  /** DELETE event */
  removeEvent(event: Event): Observable<Event> {

    const url = `${this.urlRemoveEvent}/${event.id}`;

    return this.http.delete<Event>(url, {
      headers: this.header,
    });
  }

  /** DELETE event */
  removeEventById(id: string): Observable<Event> {

    const url = `${this.urlRemoveEvent}/${id}`;

    return this.http.delete<Event>(url, {
      headers: this.header,
    });
  }


  /** PUT add user to event */
  addUserToEvent(event: Event, user: User) : Observable<Event> {

    const url = `${this.urlAddUserToEvent}/${event.id}`;
    const bodyJson = {
      idObject : user.id,
      typeObject: 'user',
    };

    return this.http.put<Event>(url, JSON.stringify(bodyJson), {
      headers: this.header,
    })
  }

  removeUserFromEvent(idEvent: string, user: User): Observable<User> {

    const url = `${this.urlRemoveUserFromEvent}/${idEvent}`;

    const bodyJson = {
      idObject : user.id,
      typeObject: 'user'
    };

    return this.http.put<User>(url, JSON.stringify(bodyJson) , {
      headers: this.header,
    });
  }

  /** GET all events from the server */
  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(this.urlEvenements)
      .pipe(catchError(this.handleError<Event[]>('getEvents', []))
      );
  }

   /** GET event by id. Will 404 if id not found */
   getEvent(id: string): Observable<Event> {
    const url = `${this.urlEvenement}/${id}`;
    return this.http.get<Event>(url)
    .pipe(catchError(this.handleError<Event>(`getEvent id=${id}`))
    );
  }

  /** GET events by user id */
  getUserEvents(idUser: string): Observable<Event[]> {
    const url = `${this.urlMembre}/${idUser}`;
    return this.http.get<Event[]>(url)
    .pipe(catchError(this.handleError<Event[]>(`getEvent id=${idUser}`))
    );
  }

  /** GET events created by user id */
  getUserCreatorEvents(idUser: string): Observable<Event[]> {
    const url = `${this.urlCreateur}/${idUser}`;
    return this.http.get<Event[]>(url)
    .pipe(catchError(this.handleError<Event[]>(`getEvent id=${idUser}`))
    );
  }
 
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      console.log('Erreur');
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}