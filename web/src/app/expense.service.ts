import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
 
import { Expense } from './model/expense';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class ExpenseService {
  //Header
  private header : HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  //URL WEB API

  //GET
  private urlExpenses = '/api/depense/getAll';
  private urlExpense = '/api/depense/get';
  private urlExpenseEvent = '/api/depense/get/event';
  private urlExpensesUser = '/api/depense/get/user';

  //POST
  private urlAddExpense = '/api/depense/add';
 
  constructor(private http: HttpClient) { }
 
    /** POST add expense */
    addExpense(expense : Expense) : Observable<Expense> {
      
      const bodyJson = {
         "userId" : expense.user.id, 
         "eventId": expense.event.id,
         "amount": expense.amount,
         "wording": expense.wording,
       };
        
      return this.http.post<Expense>(this.urlAddExpense, JSON.stringify(bodyJson), {
        headers: this.header,
       })
    }

  /** GET all expenses from the server */
  getExpenses(): Observable<Expense[]> {
    return this.http.get<Expense[]>(this.urlExpenses)
      .pipe(catchError(this.handleError<Expense[]>('getExpenses', []))
      );
  }

   /** GET expense by id. Will 404 if id not found */
   getExpense(id: string): Observable<Expense> {
    const url = `${this.urlExpense}/${id}`;
    return this.http.get<Expense>(url)
    .pipe(catchError(this.handleError<Expense>(`getExpense id=${id}`))
    );
  }

  /** GET expenses by user id */
  getUserExpenses(idUser: string): Observable<Expense[]> {
    const url = `${this.urlExpensesUser}/${idUser}`;
    return this.http.get<Expense[]>(url)
    .pipe(catchError(this.handleError<Expense[]>(`getUserDepense id=${idUser}`))
    );
  }

  /** GET expenses by event id */
  getEventExpenses(idEvent: string): Observable<Expense[]> {
    const url = `/api/depense/get/event/${idEvent}`;
    return this.http.get<any>(url)
    .pipe(catchError(this.handleError<any>(`getUserDepense id=${idEvent}`))
    );
  }
 
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      console.log('Erreur');
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}