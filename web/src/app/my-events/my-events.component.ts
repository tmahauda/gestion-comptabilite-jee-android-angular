import { GenericDialogComponent } from './../components/generic-dialog/generic-dialog.component';

import {Component, ViewChild, AfterViewInit} from '@angular/core';
import { MatPaginator, MatSort, MatTable, MatDialogConfig, MatDialog } from '@angular/material';
import {merge, Observable, of as observableOf, forkJoin} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';

import { Event } from '../model/event';
import { EventService } from './../event.service';
/**
 * @title Table retrieving data through HTTP
 */
@Component({
  selector: 'app-my-events',
  styleUrls: ['my-events.component.scss'],
  templateUrl: 'my-events.component.html',
})
export class MyEventsComponent implements AfterViewInit {
  displayedColumns: string[] = ['title', 'place', 'date', 'user', 'modifier', 'supprimer'];
  data: Event[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<Event>;

  constructor(
    private eventService: EventService,
    private dialog: MatDialog,
    ) {}

  ngAfterViewInit() {
    const idUser = localStorage.getItem('idUser');

    const observableCreatorEvent = this.eventService.getUserCreatorEvents(idUser);

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return  observableCreatorEvent;
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.data = data;
      });
  }

  public removeEvent(event: Event) {

      const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
  
      const dialogRef = this.dialog.open(GenericDialogComponent, {
        /** Données paramètre de la modale générique */
        data: {
          message: 'Voulez-vous vraiment supprimer l\'évènement ?',
          exit: 'Annuler',
          validate: 'Supprimer',
        }
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'save') {
          /** Exécute une action si l'on a confirmé avec la modale */
          if (event != null) {
            //Supprime coté serveur
            this.eventService.removeEvent(event).subscribe();
            //Supprime coté client
            this.removeEventInData(event);
            //On rafraichie la table
            this.table.renderRows();
          }
        }
      });
  }

  private removeEventInData(event: Event) {
    const index: number = this.data.indexOf(event);
    if (index >= 0) {
        this.data.splice(index, 1);
    }
  }
}
