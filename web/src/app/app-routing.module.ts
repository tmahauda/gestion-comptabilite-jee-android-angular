import { DetailEventsComponent } from './detail-events/detail-events.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { ListEventsComponent } from './list-events/list-events.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { MyEventsComponent } from './my-events/my-events.component';
import { ProfilComponent } from './profil/profil.component';
import { UserComponent } from './user/user.component';

import { ConnexionComponent } from './connexion/connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { AuthentificationGuard } from './authentification.guard';
import { NoAuthentificationGuard } from './no-authentification.guard';


const routes: Routes = [
  { path: 'connexion', component: ConnexionComponent, canActivate: [NoAuthentificationGuard]},
  { path: 'inscription', component: InscriptionComponent },
  { path: 'accueil', component: AccueilComponent, canActivate: [AuthentificationGuard]},
  { path: 'listeEvenements', component: ListEventsComponent, canActivate: [AuthentificationGuard]},
  { path: 'creationEvenement', component: CreateEventComponent, canActivate: [AuthentificationGuard]},
  { path: 'mesEvenements', component: MyEventsComponent, canActivate: [AuthentificationGuard]},
  { path: 'mesEvenements/:id', component: DetailEventsComponent, canActivate: [AuthentificationGuard]},
  { path: 'profil', component: ProfilComponent, canActivate: [AuthentificationGuard]},
  { path: 'users', component: UserComponent},
  { path: '**', redirectTo: 'accueil'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
