import { User } from './user';

export class Event {
    id: string;
    title: string;
    date: string;
    active: boolean;
    place: string;
    description: string;
    image: string;
    userList: User[];
    user: User;
}
