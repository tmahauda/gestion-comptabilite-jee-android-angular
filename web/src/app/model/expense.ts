import { Event } from '../model/event';
import { User } from '../model/user';

export class Expense {
    id: string;
    amount: number;
    wording: string;
    event: Event;
    user: User;
}
