import { GenericDialogComponent } from './../components/generic-dialog/generic-dialog.component';
import {HttpClient} from '@angular/common/http';
import {Component, ViewChild, AfterViewInit, Input, ElementRef, Inject, Injectable} from '@angular/core';
import { MatPaginator, MatSort, MatDialogConfig } from '@angular/material';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';

import {MatTableDataSource} from '@angular/material';

import { Event } from '../model/event';
import { EventService } from '../event.service';

export interface DonneesFiltrable {
  id: string;
  title: string;
  place: string;
  image: string;
  date: string;
  organisateur: string;
}

/**
 * @title Table retrieving data through HTTP
 */
@Component({
  selector: 'app-list-events',
  styleUrls: ['list-events.component.scss'],
  templateUrl: 'list-events.component.html',
})

export class ListEventsComponent implements AfterViewInit {
  displayedColumns: string[] = ['title', 'place', 'image', 'date', 'organisateur', 'modifier'];
  data: Event[] = [];
  dataFiltrable: DonneesFiltrable[] = [];
  tmp: DonneesFiltrable;
  dataSource = new MatTableDataSource(this.dataFiltrable);
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  compteur = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private http: HttpClient, private eventService: EventService) {}

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          const eventParticipants = this.eventService!.getUserEvents(localStorage.getItem('idUser'));
          return eventParticipants;
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;

          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.data = data;
        this.data.forEach(element => {
          this.compteur++;
          const date = element.date.substring(8, 10) + '/' + element.date.substring(5, 7) + '/' + element.date.substring(0, 4);
          this.tmp = {id: element.id, title: element.title, place: element.place, image:element.image, date: date, organisateur: element.user.username};
          this.dataFiltrable.push(this.tmp);
        });
        this.dataSource = new MatTableDataSource(this.dataFiltrable);
        });
  }

  applyFilter(filterValue: string) {
    //Choix des champs que l'on souhaite filtrés
    this.dataSource.filterPredicate = function(data, filter: string): boolean {
      return data.title.toLowerCase().includes(filter) || data.place.toLowerCase().includes(filter) ||
      data.date.toLowerCase().includes(filter) || data.organisateur.toLowerCase().includes(filter);
    };
    //Filtre
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.compteur = this.dataSource.filteredData.length;
  }
}
