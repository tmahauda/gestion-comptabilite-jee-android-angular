import { Router } from '@angular/router';
import { CrossFieldErrorMatcher } from './../crossFieldErrorMatcher';
import { UserService } from './../user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { LoadImageComponent } from '../components/load-image/load-image.component';

import { User } from './../model/user';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  public profilForm: FormGroup;
  hide = true;
  hideConfirm = true;
  CrossFieldErrorMatcher = new CrossFieldErrorMatcher();
  @ViewChild(LoadImageComponent) imageComponent: LoadImageComponent;
  image = '';

  constructor(
    private userService: UserService,
    private snackBar: MatSnackBar,
    public user: User,
    private fb: FormBuilder,
    private router: Router,
  ) {}

  ngOnInit() {
    this.user.id = localStorage.getItem('idUser');
    this.user.username = localStorage.getItem('pseudoUser');
    this.user.email = localStorage.getItem('emailUser');
    this.user.password = localStorage.getItem('passwordUser');
    this.user.image = localStorage.getItem('imageUser');
    this.image = this.user.image;

    this.profilForm = this.fb.group({
      pseudo: new FormControl(this.user.username, [Validators.required]),
      email: new FormControl(this.user.email, [Validators.required, Validators.email]),
      password: new FormControl(this.user.password, [Validators.required]),
      passwordConfirmation: new FormControl(this.user.password)
    },{
      validator: this.checkPasswords,
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.profilForm.controls[controlName].hasError(errorName);
  }

  public formHasError = (errorName: string) => {
    return this.profilForm.hasError(errorName);
  }

  public onSubmit = () => {
    if (this.profilForm.valid) {
      this.userService.updateUser(this.user.id, this.user.username, this.user.email, this.user.password, this.user.image).subscribe(
      (response) => {
        this.snackBar.open('Profil modifié avec succès', 'Fermer', {
          duration: 2000,
        });
        localStorage.setItem('pseudoUser', this.profilForm.value.pseudo);
        localStorage.setItem('emailUser', this.profilForm.value.email);
        localStorage.setItem('passwordUser', this.profilForm.value.password);
        localStorage.setItem('imageUser', this.imageComponent.image);
        this.ngOnInit();
        this.router.navigate(['/accueil']);
      },
      (error) => {
        const action = 'Erreur';
        this.snackBar.open(error, action, {
          duration: 2000,
        });
      }
    );
    }
  }
  checkPasswords(form: FormGroup) {
    const pass = form.get('password').value;
    const confirmPass = form.get('passwordConfirmation').value;

    return pass === confirmPass ? null : { notSame: true };
  }

}