import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EventService } from '../event.service';
import { UserService } from '../user.service';
import { ExpenseService } from '../expense.service';
import { MatSnackBar } from '@angular/material';
import { LoadImageComponent } from '../components/load-image/load-image.component';
import { MatTable } from '@angular/material';
import { MatSelect } from '@angular/material';
import { DatePipe } from '@angular/common';
import { User } from '../model/user';
import { Event } from '../model/event';
import { Expense } from '../model/expense';
import { MaterialService } from 'src/app/material.service';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss'],
})
export class CreateEventComponent implements OnInit {

  @ViewChild('tableUsers') tableUsers: MatTable<User>;
  @ViewChild('tableExpenses') tableExpenses: MatTable<Expense>;
  @ViewChild(MatSelect) select: MatSelect;
  @ViewChild(LoadImageComponent) imageComponent: LoadImageComponent;
  public eventForm: FormGroup;
  public minDate: Date;

  public displayedColumnsParticipant: string[] = ['name', 'email', 'remove'];
  public users: User[] = [];
  public participants: User[] = [];

  public displayedColumnsExpense: string[] = ['motif', 'montant', 'remove'];
  public expenses: Expense[] = [];
  public totalExpenses: number;

  constructor(
    private eventService: EventService,
    private userService: UserService,
    private expenseService: ExpenseService,
    private snackBar: MatSnackBar,
    private datepipe: DatePipe,
    private materialService: MaterialService
    ) {}

  ngOnInit() {
    this.minDate = new Date();
    this.eventForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      place: new FormControl('', [Validators.required]),
      date: new FormControl(new Date(), [Validators.required]),
      cp: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required])
    });

    this.displayedColumnsParticipant = ['name', 'email', 'remove'];
    this.getUsers();
    this.participants = [];

    this.displayedColumnsExpense = ['motif', 'montant', 'remove'];
    this.expenses = [];
    this.totalExpenses = 0;

  }

  private getUsers() {
    //On charge tous les participants sauf le créateur
    this.userService.getUsers().subscribe(users => {
      this.users = users.filter(user => user.id !== localStorage.getItem('idUser'));
    });
  }

  private isNullorEmpty(value: string) {
    return (value === '' || value === null || typeof value === 'undefined');
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.eventForm.controls[controlName].hasError(errorName);
  }

  public addParticipant(user: User) {
    this.switchArray(this.participants, this.users, user);
  }

  public removeParticipant(user: User) {
    this.switchArray(this.users, this.participants, user);
  }

  private switchArray(array1: User[], array2: User[], user: User) {
    if (user != null) {
      this.addItem<User>(array1, user);
      this.removeItem<User>(array2, user);
      /** Met à jour la table */
      this.tableUsers.renderRows();
      /** On réinitialise la valeur de sélection */
      this.select.value = null;
    }
  }

  private addItem<T>(items: T[], item: T) {
    items.push(item);
  }

  private removeItem<T>(items: T[], item: T) {
      const index: number = items.indexOf(item);
      if (index >= 0) {
          items.splice(index, 1);
      }
  }

  public addExpense(motif: any, montant: any) {
      if (!this.isNullorEmpty(motif.value) && !this.isNullorEmpty(montant.value)) {
        const expense = new Expense();
        expense.wording = motif.value;
        expense.amount = Number(montant.value);
        this.totalExpenses += expense.amount;
        this.addItem<Expense>(this.expenses, expense);
        this.tableExpenses.renderRows();
        motif.value = null;
        montant.value = null;
      }
  }

  public removeExpense(expense: Expense) {
    if(expense != null) {
      this.removeItem<Expense>(this.expenses, expense);
      this.totalExpenses -= expense.amount;
      this.tableExpenses.renderRows();
    }
  }

  public onSubmit = () => {
    if (this.eventForm.valid) {
      /** Création du user admin */
      const user = new User();
      user.id = localStorage.getItem('idUser');
      user.email = localStorage.getItem('emailUser');
      user.password = localStorage.getItem('passwordUser');
      user.username = localStorage.getItem('pseudoUser');
      user.image = localStorage.getItem('imageUser');

      /** Création de l'event */
      const event = new Event();
      event.active = true;
      event.title = this.eventForm.value.name;
      event.place = this.eventForm.value.place + ";" + this.eventForm.value.city + ";" + this.eventForm.value.cp;
      event.date = this.datepipe.transform(this.eventForm.value.date, 'yyyy-MM-dd');
      event.description = this.eventForm.value.description;
      event.image = this.imageComponent.image;
      event.user = user;

      /** Envoi du event au serveur */
      this.eventService.addEvent(event).subscribe
      (
        (newEvent) => {
          this.snackBar.open('Evénement crée', 'OK', {
            duration: 2000,
          });
          /** Pour chaque participant, on l'ajoute dans l'event crée */
          this.participants.forEach(newUser => {
            this.eventService.addUserToEvent(newEvent, newUser).subscribe(
              (response) => {
                this.snackBar.open('Participant ajouté', 'OK', {
                  duration: 2000,
                });
              },
              (error) => {
                this.snackBar.open(error, 'Erreur', {
                  duration: 2000,
                });
            });
          });

          /** On ajoute les dépenses associés à l'event */
          this.expenses.forEach(newExpense => {
              /** On associe à la dépense le user et l'event */
              newExpense.user = user;
              newExpense.event = newEvent;

              this.expenseService.addExpense(newExpense).subscribe(
                (response) => {
                  this.snackBar.open('Dépense ajouté', 'OK', {
                    duration: 2000,
                  });
                },
                (error) => {
                  this.snackBar.open(error, 'Erreur', {
                    duration: 2000,
                  });
              });
          });
        },
        (error) => {
          this.snackBar.open(error, 'Erreur', {
            duration: 2000,
          });
        }
      );
    }
  }

}
