import { Injectable } from '@angular/core';
import { MaterialModule } from './material/material.module';

@Injectable({
  providedIn: MaterialModule
})
export class MaterialService {

  constructor() { }
}
