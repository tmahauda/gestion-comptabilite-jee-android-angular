import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthentificationService } from './authentification.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NoAuthentificationGuard implements CanActivate {

  constructor(private router: Router, private authentificationService: AuthentificationService) { }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.authentificationService.isLogin()) {
      return true;
    }
    this.router.navigate(['/accueil']);
    return false;
  }
}
