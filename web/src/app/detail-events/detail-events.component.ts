import { GenericDialogComponent } from './../components/generic-dialog/generic-dialog.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EventService } from '../event.service';
import { UserService } from '../user.service';
import { ExpenseService } from '../expense.service';
import { MatSnackBar, MatDialogConfig, MatDialog, MatInput } from '@angular/material';
import { LoadImageComponent } from "../components/load-image/load-image.component"
import { MatTable } from '@angular/material';
import { MatSelect } from '@angular/material';
import { DatePipe } from '@angular/common';
import { User } from '../model/user';
import { Event } from '../model/event';
import { Expense } from '../model/expense';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detail-events',
  templateUrl: './detail-events.component.html',
  styleUrls: ['./detail-events.component.scss']
})
export class DetailEventsComponent implements OnInit {

  @ViewChild('tableUsers') tableUsers: MatTable<User>;
  @ViewChild('tableExpenses') tableExpenses: MatTable<Expense>;
  @ViewChild(MatSelect) select: MatSelect;
  @ViewChild(LoadImageComponent) imageComponent: LoadImageComponent;
  inputMotif: string;
  inputMontant: string;

  public eventForm: FormGroup;
  public minDate: Date;
  public image: string;
  public notAdmin = true;
  public inModif = false;
  public inRegister = false;
  public isExpense = false;
  public displayedColumnsParticipant: string[] = ['name', 'email', 'remove'];
  public users: User[] = [];
  public participants: User[] = [];

  public displayedColumnsExpense: string[] = ['motif', 'montant', 'remove'];
  public expenses: Expense[] = [];
  public montantTotal: number = 0;
  public idEvent: string;
  public user: User = null;
  public event: Event = null;

  constructor(
    private eventService: EventService,
    private userService: UserService,
    private expenseService: ExpenseService,
    private snackBar: MatSnackBar,
    private datepipe: DatePipe,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    ){}

  ngOnInit() {

    this.minDate = new Date();
    this.eventForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      place: new FormControl('', [Validators.required]),
      date: new FormControl(new Date(), [Validators.required]),
      cp: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      motif: new FormControl(['']),
      montant:  new FormControl(['']),
    });

    this.activatedRoute.params.subscribe(params => {
      this.idEvent = params['id'];

      this.eventService.getEvent(this.idEvent).subscribe(result => {

        this.event = result;

        if(result.user !== null && (result.user.id === localStorage.getItem('idUser'))) {
          this.notAdmin = false;
        }
        const lieu = result.place.split(';');
        console.log(result.place);
        this.eventForm.controls.name.setValue(result.title);
        this.eventForm.controls.place.setValue(lieu[0]);
        this.eventForm.controls.date.setValue(result.date);
        this.eventForm.controls.cp.setValue(lieu[2]);
        this.eventForm.controls.city.setValue(lieu[1]);
        this.eventForm.controls.description.setValue(result.description);
        this.image = result.image;
        result.userList.forEach(user => {
          this.addParticipant(user);
        });
        this.users = this.users.filter(u => {
          return !this.participants.map(e => {
            return e.id;
          }).includes(u.id);
        });

        this.expenseService.getEventExpenses(this.idEvent).subscribe(response => {
          if (response !== null) {
            response.forEach(expense => {
              this.addItem<Expense>(this.expenses, expense);
              this.tableExpenses.renderRows();
              this.montantTotal += expense.amount;
            });
          }
        });
      });
    });

    this.userService.getUsers().subscribe(users => {
      this.users = users.filter(user => user.id !== localStorage.getItem('idUser'));
    });

    //Création du user
    this.user = new User();
    this.user.id = localStorage.getItem('idUser');
    this.user.email = localStorage.getItem('emailUser');
    this.user.password = localStorage.getItem('passwordUser');
    this.user.username = localStorage.getItem('pseudoUser');
  }

  private isNullorEmpty(value : string) {
    return (value === '' || value === null || typeof value === 'undefined');
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.eventForm.controls[controlName].hasError(errorName);
  }

  public addParticipant(user : User) {
    this.switchArray(this.participants, this.users, user);
  }

  public removeParticipant(user : User) {
    this.switchArray(this.users, this.participants, user);
  }

  private switchArray(array1: User[], array2: User[], user: User) {
    if(user != null) {  
      this.addItem<User>(array1, user);
      this.removeItem<User>(array2, user);
      this.tableUsers.renderRows();
      
      if(this.select){
        this.select.value = null;
      }
    }
  }

  private addItem<T>(items: T[], item: T) {
    items.push(item);
  }

  private removeItem<T>(items: T[], item: T) {
      const index: number = items.indexOf(item);
      if (index >= 0) {
          items.splice(index, 1);
      }        
  }

  public addExpense() {
    const motif = this.eventForm.controls['motif'].value;
    const montant = this.eventForm.controls['montant'].value;

    if(motif && montant){
      if(!this.isNullorEmpty(motif) && !this.isNullorEmpty(montant)) {
        const expense = new Expense();
        expense.user = this.user;
        expense.wording = motif;
        expense.amount = Number(montant);
        this.addItem<Expense>(this.expenses, expense);
        this.tableExpenses.renderRows();
        this.montantTotal += expense.amount;
      }
    }
  }

  public removeExpense(expense : Expense) {
    this.removeItem<Expense>(this.expenses, expense);
    this.tableExpenses.renderRows();
    this.montantTotal -= expense.amount;
  }

  public quitEvent(){
    const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      const dialogRef = this.dialog.open(GenericDialogComponent, {
        /** Données paramètre de la modale générique */
        data: {
          message: 'Voulez-vous vraiment quitter l\'évènement ?',
          exit: 'Annuler',
          validate: 'Quitter',
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'save') {
          const user = new User();
          user.id = localStorage.getItem('idUser');
          this.eventService.removeUserFromEvent(this.idEvent, user).subscribe();
          this.router.navigate(['/accueil']);
        }
      });
  }

  public supprEvent(){
    const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      const dialogRef = this.dialog.open(GenericDialogComponent, {
        /** Données paramètre de la modale générique */
        data: {
          message: 'Voulez-vous vraiment supprimer l\'évènement ?',
          exit: 'Annuler',
          validate: 'Supprimer',
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'save') {
          this.eventService.removeEventById(this.idEvent).subscribe();
          this.router.navigate(['/accueil']);
        }
      });
  }
  public onSubmit = () => {
    if (this.eventForm.valid) {

      this.inModif = false;
      this.inRegister = true;

      //Création de l'event si null
      if(this.event == null) {
        this.event = new Event();
        this.event.active = true;
        this.event.title = this.eventForm.value.name;
        this.event.place = this.eventForm.value.place+";"+this.eventForm.value.city+";"+this.eventForm.value.cp;
        this.event.date = this.datepipe.transform(this.eventForm.value.date, "yyyy-MM-dd");
        this.event.description = this.eventForm.value.description;
        this.event.image = this.imageComponent.image;
        this.event.user = this.user;
      }
      
      this.eventService.removeEventById(this.idEvent).subscribe(() => {
          this.eventService.addEvent(this.event).subscribe((newEvent) => {
              this.snackBar.open("Evénement modifié", "OK", {
                duration: 2000,
              });
              this.inRegister = false;
              //Pour chaque participant, on l'ajoute dans l'event crée
              this.participants.forEach(newUser => {
                this.eventService.addUserToEvent(newEvent, newUser).subscribe(
                  (response) => {
                    this.snackBar.open("Participant ajouté", "OK", {
                      duration: 2000,
                    });
                  },
                  (error) => {
                    this.snackBar.open(error, "Erreur", {
                      duration: 2000,
                    });
                });
              });
    
              // On ajoute les dépenses associés à l'event
              this.expenses.forEach(newExpense => {
                  //On associe les dépenses au nouveau event
                  newExpense.event = newEvent;

                  this.expenseService.addExpense(newExpense).subscribe(
                    (response) => {
                      this.snackBar.open('Dépense ajouté', 'OK', {
                        duration: 2000,
                      });
                    },
                    (error) => {
                      this.snackBar.open(error, 'Erreur', {
                        duration: 2000,
                      });
                  });
              });
            },
            (error) => {
              this.snackBar.open(error, "Erreur", {
                duration: 2000,
              });
            }
          );
        }
      );
    }
  }
}
