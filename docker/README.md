# Docker_files-Groupe4

Pré-requis :
- Avoir Docker
- Avoir les ports 8080, 8081 et 4200 disponible

Etape 1 : Ouvrir un terminal et se placer dans le dossier "docker_files-groupe4"
- cd docker_files-groupe4/

Etape 2 : Lancer les images (l'application) en arrière plan avec l'option -d. Si vous voulez voir les logs, ne pas mettre -d
- docker-compose up -d

Etape 3 : Arrêter les images (l'application)
- docker-compose stop

Pour accéder au server : localhost:8080/...
Pour accéder au client : localhost:4200/...
Pour accéder à la base de données avec phpMyAdmin : localhost:8081

