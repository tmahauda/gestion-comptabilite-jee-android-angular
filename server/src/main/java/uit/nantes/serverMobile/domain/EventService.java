package uit.nantes.serverMobile.domain;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import uit.nantes.serverMobile.api.entities.Event;
import uit.nantes.serverMobile.api.entities.Expense;
import uit.nantes.serverMobile.api.entities.User;
import uit.nantes.serverMobile.api.pojo.EventPojo;
import uit.nantes.serverMobile.api.pojo.IdPojo;
import uit.nantes.serverMobile.domain.util.EventCheck;
import uit.nantes.serverMobile.domain.util.ExpenseManagement;
import uit.nantes.serverMobile.infra.jpa.IEventRepository;
import uit.nantes.serverMobile.infra.jpa.IExpenseRepository;
import uit.nantes.serverMobile.infra.jpa.IUserRepository;

/**
 * @author Daniel Clemente Aguirre
 * @author Djurdjevic Sacha
 */
@Service
public class EventService {

    @Autowired
    IEventRepository eventRepository;

    @Autowired
    IUserRepository userRepository;

    @Autowired
    IExpenseRepository expenseRepository;

    /*
    * @return La liste de tous les évènements
    */
    public List<Event> findAll() {
        return eventRepository.findAll();
    }

    /*
    * @param String id Un identifiant d'évènement
    * @return Un utilisateur correspondant à l'id rentré en paramètre
    */
    public Event findById(String id) {
        Event event = new Event();
        if (eventRepository.existsById(id)) {
            event = eventRepository.findById(id).get();
        }
        return event;
    }

    /*
    * @param String pseudo Un titre d'évènement
    * @return Un évènement correspondant au titre rentré en paramètre
    */
    public Event findByTitle(String title) {
        Event result = new Event();
        for (Event event : eventRepository.findAll()) {
            if (event.getTitle().equals(title)) {
                result = event;
            }
        }
        return result;
    }

    /*
    * @param String idUser Un identifiant d'utilisateur
    * @return Une liste d'évènement correspondant
    */
    public List<Event> findAllByUser(String idUser) {
        List<Event> result = new ArrayList<>();
        if (userRepository.existsById(idUser)) {
            for (Event event : eventRepository.findAll()) {
                for (User user : event.getUserList()) {
                    if (user.getId().equals(idUser)) {
                        result.add(event);
                    }
                }
            }
        }
        return result;
    }

    public List<Event> findAllByUserCreator(String idUser) {
        List<Event> result = new ArrayList<>();
        if (userRepository.existsById(idUser)) {
            for (Event event : eventRepository.findAll()) {
                if (event.getUser().getId().equals(idUser)) {
                    result.add(event);
                }
            }
        }
        return result;
    }

    public Event insert(EventPojo eventPojo) {
        Event event = null;
        if (EventCheck.checkInsert(eventPojo)
                && userRepository.existsById(eventPojo.getUserId())
                && userRepository.existsById(eventPojo.getUserId())) {
            event = new Event();
            event.createId();
            event.setActive(true);
            event.setTitle(eventPojo.getTitle());
            event.setDate(eventPojo.getDate());
            event.setPlace(eventPojo.getPlace());
            event.setDescription(eventPojo.getDescription());
            event.setImage(eventPojo.getImage());
            event.setUser(userRepository.findById(eventPojo.getUserId()).get());
            
            Expense expense = ExpenseManagement.createExpenseByCreating(userRepository.findById(eventPojo.getUserId()).get(),
                    event);

            eventRepository.save(event);
            expenseRepository.save(expense);
        }
        return event;
    }

    public Event update(String id, EventPojo eventPojo) {
        Event event = null;
        if (eventRepository.existsById(id)
                && userRepository.existsById(eventPojo.getUserId())) {
            event = eventRepository.findById(id).get();
            if (EventCheck.checkUpdate(eventPojo)) {
                event.setTitle(eventPojo.getTitle());
                event.setDate(eventPojo.getDate().plusDays(1));
                event.setPlace(eventPojo.getPlace());
                event.setDescription(eventPojo.getDescription());
                event.setImage(eventPojo.getImage());
                event.setUser(userRepository.findById(eventPojo.getUserId()).get());
                eventRepository.save(event);
            }
        }
        return event;
    }

    public Event addUserToEvent(String id, IdPojo idPojo) {
        Event event = null;
        if (eventRepository.existsById(id)
                && userRepository.existsById(idPojo.getIdObject())) {
            event = eventRepository.findById(id).get();
            User user = userRepository.findById(idPojo.getIdObject()).get();
            event.getUserList().add(user);
            Expense expense = ExpenseManagement.createExpenseByCreating(userRepository.findById(idPojo.getIdObject()).get(),
                    event);
            eventRepository.save(event);
            expenseRepository.save(expense);
        }
        return event;
    }

    public Event removeUserFromEvent(String id, IdPojo idPojo) {
        Event event = null;
        if (eventRepository.existsById(id)
                && userRepository.existsById(idPojo.getIdObject())) {
            event = eventRepository.findById(id).get();
            final Event eventToFind = event;
            User user = userRepository.findById(idPojo.getIdObject()).get();
            user.getExpenseList().removeIf(expensive -> 
            { 
            	if(expensive.getEvent().getId() == eventToFind.getId())
            	{
					try 
					{
	            		CloseableHttpClient httpclient = HttpClients.createDefault();
	            		HttpDelete httpDelete = new HttpDelete("http://localhost:8080/api/depense/delete/"+expensive.getId());
	            		httpDelete.getRequestLine();
	            		httpclient.execute(httpDelete);
	            	} 
					catch (Exception e) 
					{
						System.out.println(e.getMessage());
					}
            		return true;
            	}
            	return false;
            });
            event.getUserList().remove(user);
            userRepository.save(user);
            eventRepository.save(event);
        }
        return event;
    }

    public Event delete(String id) {
        Event event = null;
        if (eventRepository.existsById(id)) {
            event = eventRepository.findById(id).get();
            eventRepository.deleteById(id);
        }
        return event;
    }

}
