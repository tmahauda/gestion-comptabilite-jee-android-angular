package uit.nantes.serverMobile.application.controller;

import java.util.List;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import uit.nantes.serverMobile.api.entities.User;
import uit.nantes.serverMobile.api.pojo.UserPojo;
import uit.nantes.serverMobile.domain.EventService;
import uit.nantes.serverMobile.domain.UserService;

/**
 * @author Daniel Clemente Aguirre
 * @author Djurdjevic Sacha
 */
@Controller
@RequestMapping("/api/membre")
public class UserController {

    @Autowired
    UserService userService;
    
    @Autowired
    EventService eventService;
    
    @GetMapping(path = "/getAll")
    public @ResponseBody
    List<User> getAll() throws JSONException {
        return userService.findAll();
    }

    @GetMapping(path = "/get/{id}")
    public @ResponseBody
    User getUserById(@PathVariable String id) throws JSONException {
        return userService.findById(id);
    }

    @GetMapping(path = "/get/email/{email}")
    public @ResponseBody
    User getUserByEmail(@PathVariable String email) throws JSONException {
        return userService.findByEmail(email);
    }

    @GetMapping(path = "/get/pseudo/{pseudo}")
    public @ResponseBody
    User getUserByPseudo(@PathVariable String pseudo) throws JSONException {
        return userService.findByPseudo(pseudo);
    }

    @PutMapping(path = "/update/{id}")
    public @ResponseBody
    User updateUser(@PathVariable String id, @RequestBody UserPojo userPojo) throws JSONException {
        return userService.update(id, userPojo);  
    }

    @PostMapping(path = "/add")
    public @ResponseBody
    User addUser(@RequestBody UserPojo userPojo) throws JSONException {
        return userService.insert(userPojo);
    }
    
    @DeleteMapping(path = "/delete/{id}")
    public @ResponseBody
    User deleteUser(@PathVariable String id) throws JSONException{
    	return userService.delete(id, eventService.findAllByUser(id));
    }

}
