package uit.nantes.serverMobile.application.controller;

import java.util.List;

import org.json.JSONException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import uit.nantes.serverMobile.api.entities.Event;
import uit.nantes.serverMobile.api.pojo.EventPojo;
import uit.nantes.serverMobile.api.pojo.IdPojo;
import uit.nantes.serverMobile.domain.EventService;
import uit.nantes.serverMobile.domain.ExpenseService;
import uit.nantes.serverMobile.domain.UserService;

/**
 * @author Daniel Clemente Aguirre
 * @author Djurdjevic Sacha
 */
@RestController
@RequestMapping(value = "/api/evenement")
public class EventController {

    @Autowired
    EventService eventService;
    
    @Autowired
    ExpenseService expenseService;
    
    @Autowired
    UserService userService;

    @GetMapping(path = "/getAll")
    @ResponseBody
    public List<Event> getAll() throws JSONException {
        return eventService.findAll();
    }

    @GetMapping(path = "/get/{id}")
    @ResponseBody
    public Event getEventById(@PathVariable String id) throws JSONException {
        return eventService.findById(id);
    }

    @GetMapping(path = "/get/title/{title}")
    @ResponseBody
    public Event getEventByTitle(@PathVariable("title") String title) throws JSONException {
        return eventService.findByTitle(title);
    }

    @GetMapping(path = "/getAll/user/{id}")
    @ResponseBody
    public List<Event> getAllEventByUser(@PathVariable String id) throws JSONException {
        return eventService.findAllByUser(id);
    }

    @GetMapping(path = "/getAll/userCreator/{id}")
    @ResponseBody
    public List<Event> getAllEventByUserCreator(@PathVariable String id) throws JSONException {
        return eventService.findAllByUserCreator(id);
    }

    @PostMapping(path = "/add")
    @ResponseBody
    public Event addEvent(@RequestBody EventPojo eventPojo) throws JSONException {
    	return eventService.insert(eventPojo);
    }

    @PutMapping(path = "/update/{id}")
    @ResponseBody
    public Event updateEvent(@PathVariable String id, @RequestBody EventPojo eventPojo) throws JSONException {
        return eventService.update(id, eventPojo);
    }

    @DeleteMapping(path = "/delete/{id}")
    @ResponseBody
    public Event deleteEvent(@PathVariable String id) throws JSONException {
        return eventService.delete(id);
    }

    @PutMapping(path = "/addUser/{id}")
    @ResponseBody
    public Event addUserToEvent(@PathVariable String id, @RequestBody IdPojo idPojo) throws JSONException {
    	return eventService.addUserToEvent(id, idPojo);
    }

    @PutMapping(path = "/removeUser/{id}")
    @ResponseBody
    public Event removeUserFromEvent(@PathVariable String id, @RequestBody IdPojo idPojo) throws JSONException {
    	return eventService.removeUserFromEvent(id, idPojo);
    }

}
