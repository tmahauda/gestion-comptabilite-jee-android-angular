package uit.nantes.serverMobile.api.pojo;

/**
 * @author Daniel Clemente Aguirre
 * @author Djurdjevic Sacha
 */
public class UserPojo {

    private final String pseudo;
    private final String email;
    private final String password;
    private final String image;

    public UserPojo(String pseudo, String email, String password, String image) {
        this.pseudo = pseudo;
        this.email = email;
        this.password = password;
        this.image = image;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
    
    public String getImage() {
        return image;
    }

}
