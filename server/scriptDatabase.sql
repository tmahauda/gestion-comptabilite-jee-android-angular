-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3305
-- Généré le :  lun. 29 avr. 2019 à 14:14
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `servermobile`
--

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

CREATE TABLE `event` (
  `id_event` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `user_id_user` varchar(255) DEFAULT NULL,
  `image` longblob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `expense`
--

CREATE TABLE `expense` (
  `id` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `wording` varchar(255) DEFAULT NULL,
  `event_id_event` varchar(255) DEFAULT NULL,
  `user_id_user` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id_user` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `image` longblob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `email`, `password`, `username`, `image`) VALUES
('7d3a4fee-09d2-4325-91ee-5f21b137d9f9', 'quentin@quentin.com', 'quentin', 'quentin', NULL),
('a00c460c-42c3-4c41-880a-2e1ee012c3e3', 'corentin@corentin.com', 'corentin', 'corentin', NULL),
('a0634c55-c1a1-48b2-903c-841cf16bb3f9', 'theo@theo.com', 'theo', 'theo', NULL),
('fa300bf0-c7e5-48d7-8139-7af05cfaa1ba', 'cedric@cedric.com', 'cedric', 'cedric', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user_event`
--

CREATE TABLE `user_event` (
  `event_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id_event`),
  ADD KEY `FKcmum0fi7ofop556b5j0qq4oif` (`user_id_user`);

--
-- Index pour la table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK4quqq2acim150ndj3h3t0qf3q` (`event_id_event`),
  ADD KEY `FKetoo90fxw5g8u5gpwhpqhor0` (`user_id_user`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`),
  ADD UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`);

--
-- Index pour la table `user_event`
--
ALTER TABLE `user_event`
  ADD KEY `FKk3smcqwou8absq8qjt3wk4vy9` (`user_id`),
  ADD KEY `FKspe8srtv69gubpphvrnd7wekt` (`event_id`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `FKcmum0fi7ofop556b5j0qq4oif` FOREIGN KEY (`user_id_user`) REFERENCES `user` (`id_user`);

--
-- Contraintes pour la table `expense`
--
ALTER TABLE `expense`
  ADD CONSTRAINT `FK4quqq2acim150ndj3h3t0qf3q` FOREIGN KEY (`event_id_event`) REFERENCES `event` (`id_event`),
  ADD CONSTRAINT `FKetoo90fxw5g8u5gpwhpqhor0` FOREIGN KEY (`user_id_user`) REFERENCES `user` (`id_user`);

--
-- Contraintes pour la table `user_event`
--
ALTER TABLE `user_event`
  ADD CONSTRAINT `FKk3smcqwou8absq8qjt3wk4vy9` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `FKspe8srtv69gubpphvrnd7wekt` FOREIGN KEY (`event_id`) REFERENCES `event` (`id_event`);
